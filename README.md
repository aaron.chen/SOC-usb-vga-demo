# SOC-usb-vga-demo
(implementation of Lab 8 for ECE 385)

### By Aaron Chen, Shruti Chanumolu
Protocol to interface a keyboard and a monitor with the DE2 board using the on-board USB and VGA ports.

In this lab we wrote a protocol to interface a keyboard and a monitor with the DE2 board using the on-board USB and VGA ports. Our aim was to control the direction of a bouncing ball using the keycodes. To implement the above, we  first created a low-level interface between NIOS II and USB chip (CY7C67200 “EZ-OTG”) . We then connected USB keyboard to “USB Host” port on DE2-115  to enumerate & read key-codes and, VGA controller on monitor  to VGA port. Finally we demonstrate a ball on the monitor  whose movement is controlled by the user inputs from the keyboard.

In this lab, we use Nios II embedded processor  for the USB enumeration for a HID device.   The USB protocol is handled in the software on the Nios II, and the extracted keycode from the USB keyboard is then sent to the hardware for further use . 
The Cypress EZ-OTG (CY7C67200) chip handles this  USB protocol.  The connections between EZ-OTG and the DE2-115 FPGA board are made through the Host Port Interface (HPI ). We  need to program and configure EZ-OTG in order to make it act as a Host Controller and establish  connection with the USB keyboard. Therefore we write a C program to do the following 
1.Set up the EZ-OTG chip.(Io_read and IO_write operations are used to communicate with the EG-OTP chip as explained in detail later)
2. Detect connection and assign an address to the connected device.
3. Identify device type from descriptors.
4. Set configuration.
5. Poll keyboard data.

We set up Qsys to define PIO’s (parallel input output connections) for address, data, read, write and chip select  so that the NIOS II can interface with the System Verilog hardware I/O wrapper to control the CY7C67200 chip that handles USB input and output . The  hardware I/O wrapper in System Verilog  handles the connections between the PIO’s coming from the NIOS II processor to the pins that control the CY7C67200 chip. The data bus will need to be tri-stated, as it can be driven both by the NIOS and by the CY7C67200 chip. 

<img src="soc_flow.png" alt="soc_flow" />

The C program we wrote helps poll data from the keyboard and these keycodes are sent to the top level and ball.sv module in system verilog. Based on the keycode these modules will determine the display and ball movement  on the VGA monitor. 

### Block Diagram

<img src="block_diagram.png" alt="block_diagram" />

lab8.sv should be set as top level.
